## [1.1.1](https://gitlab.com/czerasz/go-terraform/compare/v1.1.0...v1.1.1) (2020-08-04)


### Configuration

* set default Terraform version to 0.12.29 ([0efff26](https://gitlab.com/czerasz/go-terraform/commit/0efff264fa79a9df9040625db5806d434a91ce62))

# [1.1.0](https://gitlab.com/czerasz/go-terraform/compare/v1.0.1...v1.1.0) (2020-07-14)


### Features

* add destroy ([35e61c1](https://gitlab.com/czerasz/go-terraform/commit/35e61c15e6e7b9b0d829aba5085ee0f5eb8f0344))

## [1.0.1](https://gitlab.com/czerasz/go-terraform/compare/v1.0.0...v1.0.1) (2020-07-13)


### Bug Fixes

* set default stdout and stderr during run ([7cf8f81](https://gitlab.com/czerasz/go-terraform/commit/7cf8f81143302f243aef5a212a09dfda6728a633))


### Code Refactoring

* increase download timeout ([c9837dc](https://gitlab.com/czerasz/go-terraform/commit/c9837dc271c90be53ca30b1093009353378a4c7f))
* simplify basic example ([9e3a94d](https://gitlab.com/czerasz/go-terraform/commit/9e3a94dc725004ff45a0a3d6bcddfb6305a0c477))


### Continuous Integrations

* improve release configuration ([54cd55e](https://gitlab.com/czerasz/go-terraform/commit/54cd55e4fecd0cf51da14c4939622abb550cf3e8))


### Documentation

* improve examples documentation ([09e90d0](https://gitlab.com/czerasz/go-terraform/commit/09e90d057a07ac9ad867daf786f50aa66234ea86))

# 1.0.0 (2020-07-08)


### chore

* initialize project - add go.mod ([f474b8a](https://gitlab.com/czerasz/go-terraform/commit/f474b8a4c21b1462fd6b575a0d34380e5e18e6fc))


### ci

* add CI configuration ([c94988a](https://gitlab.com/czerasz/go-terraform/commit/c94988a5a16a63805e0a34b827dbaf99e1785669))
* add missing release configuration ([bbc67ec](https://gitlab.com/czerasz/go-terraform/commit/bbc67ec2fe3027a41a28d194c7d875ab2056fd92))
* disable obsolete logs ([393a497](https://gitlab.com/czerasz/go-terraform/commit/393a49774c56b685c07658403aeabd1b4da3a5b2))
* fix missing gcc package in CI ([770a997](https://gitlab.com/czerasz/go-terraform/commit/770a997642b7803113e3e57ad38cf5bac4e83464))
* fix missing Golang in CI ([40f9f17](https://gitlab.com/czerasz/go-terraform/commit/40f9f172a65f26b5323ebc4dafb6f73cd83d711d))


### Code Refactoring

* basic example ([47a0ce9](https://gitlab.com/czerasz/go-terraform/commit/47a0ce9c4d6dad1d52f0374161c87f81faa18f34))
* fix all possible linting issues ([cbb520a](https://gitlab.com/czerasz/go-terraform/commit/cbb520ad7b3c3fd00fea15c4feeebc37be48ad52))
* fix linting issues ([442fb21](https://gitlab.com/czerasz/go-terraform/commit/442fb219f99a382e87dc92e92a79ff896e6c8c2f))
* fix wsl linter issues ([4cd6722](https://gitlab.com/czerasz/go-terraform/commit/4cd67225ce2eb184df0b23da32a2b069eea6808c))


### Documentation

* add basic example ([5b0176a](https://gitlab.com/czerasz/go-terraform/commit/5b0176a62a127ec2df124fce2249feeedeea1ab8))
* add initial documentation ([9abe95c](https://gitlab.com/czerasz/go-terraform/commit/9abe95c93e87056cd96e6bfbeb73f2f04172cfd8))


### Features

* add basic functionality ([09fc05d](https://gitlab.com/czerasz/go-terraform/commit/09fc05d44fc46645ffd2d28dfd6087135896624b))
* add download feature ([b478678](https://gitlab.com/czerasz/go-terraform/commit/b478678f884be6d28e091008b5b10d98fdd4e535))
* add run feature ([d37d5ca](https://gitlab.com/czerasz/go-terraform/commit/d37d5ca8512b2b2f2a29f18adc0d14977772a525))
