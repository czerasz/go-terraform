package main

import (
	"encoding/json"
	"fmt"

	"gitlab.com/czerasz/go-terraform/terraform"
)

type Result struct {
	Name struct {
		Value string `json:"value"`
	} `json:"name_uppercase"`
}

func main() {
	tfPath, err := terraform.Download([]terraform.DownloadSetting{}...)

	stdout := []byte{}
	stderr := []byte{}

	settings := []terraform.RunSetting{
		terraform.RunWithTerraformPath(tfPath),
		terraform.RunWithModuleDir("module/test"),
		terraform.RunWithStdout(&stdout),
		terraform.RunWithStderr(&stderr),
		terraform.RunWithApplyArgs([]string{
			`-var`, `id=b6770221-380b-4d68-8d23-663b32c4691c`,
		}),
	}

	out, err := terraform.Run(settings...)
	if err != nil {
		fmt.Printf("error: %s\n", err)
		panic(err)
	}

	fmt.Printf("Terraform raw output:\n%s\n", string(out))
	fmt.Printf("--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- \n\n")

	var r Result
	err = json.Unmarshal(out, &r)
	if err != nil {
		fmt.Printf("error: %s\n", err)
		panic(err)
	}

	fmt.Printf("Terraform output \"name_uppercase\" variable: %s\n", r.Name.Value)
	fmt.Printf("\n--- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- \n")

	debug := true
	if debug {
		fmt.Printf("\nView Output\n")
		fmt.Printf("STDERR: \n%s\n", string(stderr))
		fmt.Printf("STDOUT: \n%s\n", string(stdout))
	}
}
