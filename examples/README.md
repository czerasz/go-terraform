# Examples

## Basic

**Requirements**:

- `terraform` executable needs to be available

  ```bash
  $ which terraform 
  /usr/local/bin/terraform
  ```

**Usage**

```bash
go run basic.go
```

## Download

**Usage**

```bash
go run download.go
```

## Run

**Usage**

```bash
go run run.go
```