package main

import (
	"fmt"

	"gitlab.com/czerasz/go-terraform/terraform"
)

func main() {
	// in this example Terraform needs to be in execution path
	t := terraform.New()
	t.ModulePath = "module/test"

	anyPanic(t.Init([]string{}, []string{}), t.Plan([]string{}), t.Apply([]string{`-var`, `id=b6770221-380b-4d68-8d23-663b32c4691c`}))

	out, err := t.Output([]string{})
	anyPanic(err)
	fmt.Printf("Output:\n%s\n", string(out))

	anyPanic(t.Destroy([]string{}))
}

func anyPanic(errors ...error) {
	for _, err := range errors {
		if err != nil {
			panic(err)
		}
	}
}
