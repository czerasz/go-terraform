variable "id" {
  type        = string
  description = "test variable"
}

locals {
  id_uppercase = upper(var.id)
}

output "name_uppercase" {
  value       = local.id_uppercase
  description = "name in uppercase"
}