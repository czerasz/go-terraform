package main

import (
	"fmt"

	"gitlab.com/czerasz/go-terraform/terraform"
)

func main() {
	// Download Terraform to /tmp/
	settings := []terraform.DownloadSetting{terraform.WithDir("/tmp/")}
	path, err := terraform.Download(settings...)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Terraform path: %s", path)
}
