package terraform

import (
	"archive/zip"
	"bufio"
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"regexp"
	"runtime"
	"strings"
	"time"

	"golang.org/x/crypto/openpgp"
)

type downloadConfig struct {
	version string
	path    string // binary path
	dir     string // binary directory path
	name    string // binary name
	client  http.Client
}

// generatePath generates the binary path
func (c *downloadConfig) generatePath() error {
	// if no directory is given use cwd (current working directory)
	if c.dir == "" {
		cwd, err := os.Getwd()
		if err != nil {
			return err
		}

		c.dir = cwd
	}

	// if no binary name is given generate one
	if c.name == "" {
		c.name = fmt.Sprintf("terraform_%s_%s_%s", c.version, runtime.GOOS, runtime.GOARCH)
	}

	// set the binary path
	c.path = path.Join(c.dir, c.name)

	return nil
}

// DownloadSetting is the type for the allowed settings
type DownloadSetting func(*downloadConfig) error

// applySettings applies specified settings
func (c *downloadConfig) applySettings(stngs ...DownloadSetting) error {
	for _, s := range stngs {
		err := s(c)
		return err
	}

	return nil
}

// WithVersion defines the Terraform version
func WithVersion(version string) DownloadSetting {
	return func(c *downloadConfig) error {
		conf.version = version
		return nil
	}
}

// WithDir defines where the Terraform binary should be stored
func WithDir(dir string) DownloadSetting {
	return func(c *downloadConfig) error {
		if strings.TrimSpace(dir) == "" {
			return fmt.Errorf("dir string can not be empty")
		}

		c.dir = dir

		return nil
	}
}

// WithDir defines the Terraform file name
func WithFileName(name string) DownloadSetting {
	return func(c *downloadConfig) error {
		if strings.TrimSpace(name) == "" {
			return fmt.Errorf("name can not be empty")
		}

		c.name = name

		return nil
	}
}

// Default Terraform version
var defaultVersion = "0.12.29"

// Validate this here:
// - https://www.hashicorp.com/security
// - https://keybase.io/hashicorp
var defaultHashiCorpGPPKey = `-----BEGIN PGP PUBLIC KEY BLOCK-----

mQENBFMORM0BCADBRyKO1MhCirazOSVwcfTr1xUxjPvfxD3hjUwHtjsOy/bT6p9f
W2mRPfwnq2JB5As+paL3UGDsSRDnK9KAxQb0NNF4+eVhr/EJ18s3wwXXDMjpIifq
fIm2WyH3G+aRLTLPIpscUNKDyxFOUbsmgXAmJ46Re1fn8uKxKRHbfa39aeuEYWFA
3drdL1WoUngvED7f+RnKBK2G6ZEpO+LDovQk19xGjiMTtPJrjMjZJ3QXqPvx5wca
KSZLr4lMTuoTI/ZXyZy5bD4tShiZz6KcyX27cD70q2iRcEZ0poLKHyEIDAi3TM5k
SwbbWBFd5RNPOR0qzrb/0p9ksKK48IIfH2FvABEBAAG0K0hhc2hpQ29ycCBTZWN1
cml0eSA8c2VjdXJpdHlAaGFzaGljb3JwLmNvbT6JAU4EEwEKADgWIQSRpuf4XQXG
VjC+8YlRhS2HNI/8TAUCXn0BIQIbAwULCQgHAgYVCgkICwIEFgIDAQIeAQIXgAAK
CRBRhS2HNI/8TJITCACT2Zu2l8Jo/YLQMs+iYsC3gn5qJE/qf60VWpOnP0LG24rj
k3j4ET5P2ow/o9lQNCM/fJrEB2CwhnlvbrLbNBbt2e35QVWvvxwFZwVcoBQXTXdT
+G2cKS2Snc0bhNF7jcPX1zau8gxLurxQBaRdoL38XQ41aKfdOjEico4ZxQYSrOoC
RbF6FODXj+ZL8CzJFa2Sd0rHAROHoF7WhKOvTrg1u8JvHrSgvLYGBHQZUV23cmXH
yvzITl5jFzORf9TUdSv8tnuAnNsOV4vOA6lj61Z3/0Vgor+ZByfiznonPHQtKYtY
kac1M/Dq2xZYiSf0tDFywgUDIF/IyS348wKmnDGjuQENBFMORM0BCADWj1GNOP4O
wJmJDjI2gmeok6fYQeUbI/+Hnv5Z/cAK80Tvft3noy1oedxaDdazvrLu7YlyQOWA
M1curbqJa6ozPAwc7T8XSwWxIuFfo9rStHQE3QUARxIdziQKTtlAbXI2mQU99c6x
vSueQ/gq3ICFRBwCmPAm+JCwZG+cDLJJ/g6wEilNATSFdakbMX4lHUB2X0qradNO
J66pdZWxTCxRLomPBWa5JEPanbosaJk0+n9+P6ImPiWpt8wiu0Qzfzo7loXiDxo/
0G8fSbjYsIF+skY+zhNbY1MenfIPctB9X5iyW291mWW7rhhZyuqqxN2xnmPPgFmi
QGd+8KVodadHABEBAAGJATwEGAECACYCGwwWIQSRpuf4XQXGVjC+8YlRhS2HNI/8
TAUCXn0BRAUJEvOKdwAKCRBRhS2HNI/8TEzUB/9pEHVwtTxL8+VRq559Q0tPOIOb
h3b+GroZRQGq/tcQDVbYOO6cyRMR9IohVJk0b9wnnUHoZpoA4H79UUfIB4sZngma
enL/9magP1uAHxPxEa5i/yYqR0MYfz4+PGdvqyj91NrkZm3WIpwzqW/KZp8YnD77
VzGVodT8xqAoHW+bHiza9Jmm9Rkf5/0i0JY7GXoJgk4QBG/Fcp0OR5NUWxN3PEM0
dpeiU4GI5wOz5RAIOvSv7u1h0ZxMnJG4B4MKniIAr4yD7WYYZh/VxEPeiS/E1CVx
qHV5VVCoEIoYVHIuFIyFu1lIcei53VD6V690rmn0bp4A5hs+kErhThvkok3c
=+mCN
-----END PGP PUBLIC KEY BLOCK-----`

const defaultHTTPTimeout = 30

// Initialize default configuration
var conf = downloadConfig{
	version: defaultVersion,
	client: http.Client{
		Timeout: defaultHTTPTimeout * time.Second,
	},
}

type downloadPair struct {
	URL  string
	Path string
}

type downloads struct {
	archive     downloadPair
	archiveName string
	checksums   downloadPair
	signature   downloadPair
	client      *http.Client
}

func (d *downloads) toSlice() []downloadPair {
	return []downloadPair{
		d.archive,
		d.checksums,
		d.signature,
	}
}

// download downloads required files
func (d *downloads) download() (err error) {
	for _, file := range d.toSlice() {
		err = downloadURL(d.client, file.Path, file.URL)
		if err != nil {
			return
		}
	}

	return
}

// cleanupDownloads removes obsolete files
func (d *downloads) cleanupDownload() {
	for _, file := range d.toSlice() {
		err := os.Remove(file.Path)
		if err != nil {
			// TODO: try to remove more files or fail fast
			// This method is used with defer so error is not vital
			return
		}
	}
}

func newDownloads(version, path string, client *http.Client) *downloads {
	d := &downloads{
		client: &conf.client,
	}

	if client != nil {
		d.client = client
	}

	d.archiveName = fmt.Sprintf("terraform_%s_%s_%s.zip", version, runtime.GOOS, runtime.GOARCH)

	d.archive = downloadPair{
		URL:  fmt.Sprintf("https://releases.hashicorp.com/terraform/%s/%s", version, d.archiveName),
		Path: path + ".zip",
	}
	d.checksums = downloadPair{
		URL:  fmt.Sprintf("https://releases.hashicorp.com/terraform/%s/terraform_%s_SHA256SUMS", version, version),
		Path: path + "_SHA256SUMS",
	}
	d.signature = downloadPair{
		URL:  fmt.Sprintf("https://releases.hashicorp.com/terraform/%s/terraform_%s_SHA256SUMS.sig", version, version),
		Path: path + "_SHA256SUMS.sig",
	}

	return d
}

// Download triggers the Terraform binary download
func Download(stngs ...DownloadSetting) (string, error) {
	err := conf.applySettings(stngs...)
	if err != nil {
		return "", err
	}

	err = conf.generatePath()
	if err != nil {
		return "", err
	}

	info, err := os.Stat(conf.path)

	switch {
	case err == nil:
		// Path exists
		if !info.IsDir() {
			// Terraform file already exists - no need to continue
			return conf.path, nil
		}

		return conf.path, fmt.Errorf("specified path is an directory")
	case os.IsNotExist(err):
		// Continue if the file does NOT exist
	default:
		return conf.path, err
	}

	dwns := newDownloads(conf.version, conf.path, &conf.client)
	err = dwns.download()

	if err != nil {
		return conf.path, err
	}

	defer dwns.cleanupDownload()

	err = validSignature(dwns.checksums.Path, dwns.signature.Path)
	if err != nil {
		return conf.path, err
	}

	sum, err := extractChecksum(dwns.checksums.Path, dwns.archiveName)
	if err != nil {
		return conf.path, err
	}

	err = validChecksum(dwns.archive.Path, sum)
	if err != nil {
		return conf.path, err
	}

	err = unzip(dwns.archive.Path, conf.path)
	if err != nil {
		return conf.path, err
	}

	return conf.path, nil
}

// downloadURL downloads urls content to a local file.
// It will write as it downloads and not store the file in memory.
func downloadURL(client *http.Client, path string, url string) error {
	// Fetch data
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return err
	}

	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	// Create the file
	out, err := os.Create(path)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write content to file
	_, err = io.Copy(out, res.Body)

	return err
}

// extractChecksum finds the checksum specific to the provided archive name
func extractChecksum(checksumsPath, archiveName string) (sum []byte, err error) {
	// Prepare regular expression
	// Below example line from a checksum file:
	// be99da1439a60942b8d23f63eba1ea05ff42160744116e84f46fc24f1a8011b6  terraform_0.12.28_linux_amd64.zip
	r, err := regexp.Compile(`^([a-z0-9]+)\s+` + archiveName)
	if err != nil {
		return sum, err
	}

	file, err := os.Open(checksumsPath)
	if err != nil {
		return sum, err
	}
	defer file.Close()

	// Find the line witch matches the regular expression
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		matches := r.FindSubmatch(scanner.Bytes())
		if len(matches) > 1 {
			sum = matches[1]
			break
		}
	}

	if err := scanner.Err(); err != nil {
		return sum, err
	}

	return sum, nil
}

// validSignature validates the gpg signature
// Resource: https://gist.github.com/lsowen/d420a64821414cd2adfb
func validSignature(targetPath, sigPath string) error {
	keyRingReader := strings.NewReader(defaultHashiCorpGPPKey)

	signature, err := os.Open(sigPath)
	if err != nil {
		return err
	}

	verificationTarget, err := os.Open(targetPath)
	if err != nil {
		return err
	}

	keyring, err := openpgp.ReadArmoredKeyRing(keyRingReader)
	if err != nil {
		return err
	}

	_, err = openpgp.CheckDetachedSignature(keyring, verificationTarget, signature)
	if err != nil {
		return err
	}

	return nil
}

// validChecksum validates the SHA256 checksum
func validChecksum(path string, wanted []byte) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		return err
	}

	// Calculate actual checksum
	rawSum := h.Sum(nil)
	// Generate raw hex value
	sum := make([]byte, hex.EncodedLen(len(rawSum)))
	hex.Encode(sum, rawSum)

	// Compare checksums
	if !bytes.Equal(sum, wanted) {
		return fmt.Errorf("checksums do not match: %s != %s", sum, wanted)
	}

	return nil
}

// Resource: https://golangcode.com/unzip-files-in-go/
// unzip will decompress the zip archive
// it will move the "terraform" file to the specified path
func unzip(src string, dst string) error {
	r, err := zip.OpenReader(src)
	if err != nil {
		return err
	}
	defer r.Close()

	var exists bool

	for _, f := range r.File {
		if f.Name == "terraform" {
			exists = true

			dstFile, err := os.OpenFile(dst, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
			if err != nil {
				return err
			}

			rc, err := f.Open()
			if err != nil {
				return err
			}

			_, err = io.Copy(dstFile, rc)

			// Close the file without defer to close before next iteration of loop
			dstFile.Close()
			rc.Close()

			if err != nil {
				return err
			}
		}
	}

	if !exists {
		return fmt.Errorf("terraform file does not exist in archive")
	}

	return nil
}
