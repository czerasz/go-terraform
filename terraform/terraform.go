package terraform

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
)

// Terraform struct
type Terraform struct {
	Path       string
	ModulePath string
	Stderr     *[]byte
	Stdout     *[]byte
}

// New creates new Terraform instance with default values
func New() *Terraform {
	stdout := []byte{}
	stderr := []byte{}

	return &Terraform{
		Path:   "terraform",
		Stdout: &stdout,
		Stderr: &stderr,
	}
}

// Init executes the terraform init command
func (t *Terraform) Init(args, initEnv []string) error {
	err := t.runCommand("init", args, initEnv, nil)
	if err != nil {
		return fmt.Errorf("error during init command: %w", err)
	}

	return nil
}

// Plan executes the terraform plan command
func (t *Terraform) Plan(args []string) error {
	err := t.runCommand("plan", args, []string{}, nil)
	if err != nil {
		return fmt.Errorf("error during plan command: %w", err)
	}

	return nil
}

// Apply executes the terraform apply command
func (t *Terraform) Apply(args []string) error {
	args = append(args, "-auto-approve")

	err := t.runCommand("apply", args, []string{}, nil)
	if err != nil {
		return fmt.Errorf("error during apply command: %w", err)
	}

	return nil
}

// Destroy executes the terraform destroy
func (t *Terraform) Destroy(args []string) error {
	args = append(args, "-auto-approve")

	err := t.runCommand("destroy", args, []string{}, nil)
	if err != nil {
		return fmt.Errorf("error during destroy command: %w", err)
	}

	return nil
}

// Output executes the terraform output command and returns the JSON
func (t *Terraform) Output(args []string) ([]byte, error) {
	var stdout bytes.Buffer

	args = append(args, "-json")

	err := t.runCommand("output", args, []string{}, &stdout)
	if err != nil {
		return nil, fmt.Errorf("error during output command: %w", err)
	}

	return stdout.Bytes(), nil
}

// runCommand executes the specified terraform command
func (t *Terraform) runCommand(cmd string, args, additionalEnv []string, stdout *bytes.Buffer) error {
	if stdout == nil {
		stdout = &bytes.Buffer{}
	}

	stderr := &bytes.Buffer{}

	// Prepare the command
	c := exec.Command(t.Path, append([]string{cmd}, args...)...)
	c.Stderr = stderr
	c.Stdout = stdout
	c.Dir = t.ModulePath

	if len(additionalEnv) > 0 {
		c.Env = append(os.Environ(), additionalEnv...)
	}
	// Execute the terraform command
	err := c.Run()

	*(t.Stderr) = append(*(t.Stderr), stderr.Bytes()...)
	*(t.Stdout) = append(*(t.Stdout), stdout.Bytes()...)

	if err != nil {
		return err
	}

	return nil
}
