package terraform

import (
	"fmt"
)

type runConfig struct {
	path            string
	workDir         string
	pluginCachePath string
	initArgs        []string
	applyArgs       []string
	outputArgs      []string
	stderr          *[]byte
	stdout          *[]byte
}

// Validate the run configuration
func (c *runConfig) validate() error {
	if c.path == "" {
		return fmt.Errorf("terrafor path not set")
	}

	if c.workDir == "" {
		return fmt.Errorf("working directory not set")
	}

	return nil
}

// RunSetting is the type for the allowed settings
type RunSetting func(*runConfig) error

// applySettings applies specified settings
func (c *runConfig) applySettings(stngs ...RunSetting) error {
	for _, s := range stngs {
		err := s(c)
		if err != nil {
			return err
		}
	}

	return nil
}

// RunWithTerraformPath defines the Terraform binary path
func RunWithTerraformPath(path string) RunSetting {
	return func(c *runConfig) error {
		if path == "" {
			return fmt.Errorf("path to Terraform binary can not be empty")
		}

		c.path = path

		return nil
	}
}

// RunWithModuleDir defines terraform module path
func RunWithModuleDir(path string) RunSetting {
	return func(c *runConfig) error {
		if path == "" {
			return fmt.Errorf("module directory can not be empty")
		}

		c.workDir = path

		return nil
	}
}

// RunWithApplyArgs defines arguments passed over to terraform apply
func RunWithApplyArgs(args []string) RunSetting {
	return func(c *runConfig) error {
		c.applyArgs = args
		return nil
	}
}

// RunWithApplyArgs defines the provider plugin cache path
// Read more about provider plugin cache:
// https://www.terraform.io/docs/configuration/providers.html#provider-plugin-cache
func RunWithPluginCache(path string) RunSetting {
	return func(c *runConfig) error {
		c.pluginCachePath = path
		return nil
	}
}

// RunWithStdout defines the stdout source
func RunWithStdout(b *[]byte) RunSetting {
	return func(c *runConfig) error {
		c.stdout = b
		return nil
	}
}

// RunWithStderr defines the stderr source
func RunWithStderr(b *[]byte) RunSetting {
	return func(c *runConfig) error {
		c.stderr = b
		return nil
	}
}

// Run executes terraform init and apply
func Run(stngs ...RunSetting) (out []byte, err error) {
	var conf = runConfig{}
	err = conf.applySettings(stngs...)

	if err != nil {
		return out, err
	}

	err = conf.validate()
	if err != nil {
		return out, err
	}

	stdout := []byte{}
	stderr := []byte{}

	t := Terraform{
		Path:       conf.path,
		ModulePath: conf.workDir,
		Stdout:     &stdout,
		Stderr:     &stderr,
	}
	if conf.stdout != nil {
		t.Stdout = conf.stdout
	}

	if conf.stderr != nil {
		t.Stderr = conf.stderr
	}

	initEnv := []string{}
	if conf.pluginCachePath != "" {
		initEnv = append(initEnv, fmt.Sprintf("TF_PLUGIN_CACHE_DIR=%s", conf.pluginCachePath))
	}

	err = t.Init(conf.initArgs, initEnv)
	if err != nil {
		return out, err
	}

	err = t.Apply(conf.applyArgs)
	if err != nil {
		return out, err
	}

	out, err = t.Output(conf.outputArgs)

	return out, err
}
