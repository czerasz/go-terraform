# Interact with Terraform within Golang

This module provides the following capibilities:

- download Terraform
- Terraform commands abstraction - currently very primitive

## Examples

Find examples in the [`examples` directory](examples/)